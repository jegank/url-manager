function parser(match, p1, p2, p3, offset, fullString) {
  return construct(match, p1, p2);
}

function construct(type, path, params) {

  //throw new Error('type------>' + type + 'path---->' + path + "params---->" + params)
  //path---->service     params---->/v1/p/d
  // if(type !== 'undefined' || path !== 'undefined') {
  //   throw type;
  // }

  let config = _getCurrentConfig();
  let url = config[path.toLowerCase()];
  // if(path && path.charAt(0) === '/'){
  //   path = path.substr(1);
  // }
  url += params;
  // if (params && params.length > 0) {
  //   if (params.indexOf("?") === 0) {
  //     url += params;
  //   } else {
  //     url += '?' + params;
  //   }
  // }

  return url;
}

function _getCurrentConfig() {
  return {
    service: "http://localhost:8082"
  }
}

module.exports = parser



/*function replace (source, options) {
  const { replace, flags, strict } = options
  const search = (
    flags === null
      ? options.search
      : new RegExp(options.search, flags)
  )

  if (strict && (search === null || replace === null)) {
    throw new Error('Replace failed (strict mode) : options.search and options.replace are required')
  }

  const newSource = source.replace(search, replace)

  if (strict && (newSource === source)) {
    throw new Error('Replace failed (strict mode) : ' + options.search + ' → ' + options.replace)
  }

  return newSource
}

module.exports = replace*/
